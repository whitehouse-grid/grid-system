from flask import Flask, jsonify, request
from libs import git_handler
app = Flask(__name__)


@app.route('/service/status')
def service_status():
    resp = dict()
    resp['status'] = 200
    resp['reason'] = 'Success'
    return jsonify(resp)

"""
@app.route('/events/git/<repo>/push/')
def git_repo_push_event(repo):
    update_repo(repo)
    return "Hello World!"
"""

@app.route('/project/<name>', methods=['GET', 'POST'])
def project():
    if request.method == 'POST':
    return "Hello World!"

if __name__ == '__main__':
    app.run()