import yaml
import os


class Project:

    def __int__(self, project_path, name=None):
        self.project_path = project_path
        self.name = name
        self.metadata = self._load_metadata()

    def _load_metadata(self):
        try:
            _metadata_path = self.project_path + '/metadata.yml'
            _stream = file(_metadata_path,'r')
            metadata = yaml.load(_stream)
            return metadata
        except yaml.YAMLError:
            return None

    def get_metadata(self):
        return self.metadata

    def is_valid_project(self):
        if self.metadata is not None:
            try:
                if self.metadata['git'] is None or self.metadata['command'] is None:
                    return False
                else:
                    return True
            except KeyError:
                return False
        else:
            return False

    def setup_virtualenv():
        _env_path = self.project_path + '/env'
        os.system("virtualenv " + _env_path)

    def get_exe_path():
        return env_path = self.project_path + '/env/bin/python.exe'

    def _get_activate_virtual_env_path():
        env_path = self.project_path + '/env/Script/activate'

    def install_dependencies():
         os.system("source " + self._get_activate_virtual_env_path() + " & pip install -r " + self.project_path + "/requirements.txt ")

    def update_repo(repo):
        #TODO: Check for valid repo
        g = git.cmd.Git(get_repo_loc(repo))
        g.pull()
