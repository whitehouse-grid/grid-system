import os
import re
import git
from .project import Project

PROJECTS_DIR_NAME = './projects'


def get_projects():
    _projects_dir_path = os.path.join(os.getcwd(), PROJECTS_DIR_NAME)
    dirname, dirnames, filenames = os.walk(_projects_dir_path)
    _project = dict()
    _projects = list()

    for subdir_name in dirnames:
        _project.clear()
        _project['name'] = subdir_name
        _project['path'] = os.path.join(dirname, subdir_name)
        _projects.append(_project)
        return _projects


def get_project(project_name):
    _project = dict()
    _projects_dir_path = os.path.join(os.getcwd(), PROJECTS_DIR_NAME)
    dirname, dirnames, filenames = os.walk(_projects_dir_path)
    for subdir_name in dirnames:
        if subdir_name == project_name:
            _project['name'] = subdir_name
            _project['path'] = os.path.join(dirname, subdir_name)
            return _project
        return None


"""
Add Project to System
"""


def add_project(project_git_url):
    _result = re.search(r'/(.*)\.git', project_git_url)
    _project_name = _result.group(1)

    # Search for existing project
    if get_project(_project_name) is not None:
        # TODO:Delete project
        return None

    # Create new project -> Git
    git.Git(PROJECTS_DIR_NAME).clone(project_git_url)
    _project = get_project(_project_name)
    if _project is None:
        return None
    project = Project(_project['path'], _project['name'])

    # Check if project meets min requirements
    if not project.is_valid_project():
        # TODO:Delete project
        return None

# TODO: Install requirements.txt

# TODO: Identify Start Command and Start Service
