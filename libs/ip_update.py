#!/usr/bin/python
 
import urllib2
from os.path import expanduser
import os.path
 
OLDIP_FILE = expanduser("~") + "/dynamic-ip"
SECRET_KEY = 'TVd1VmlVZ2J1bEh4VUNNcFExQW45WTRXOjE3ODU4ODEz'
 
def updatedns(ip):
    print urllib2.urlopen("http://freedns.afraid.org/dynamic/update.php?" + SECRET_KEY).read().strip()
    f = open(OLDIP_FILE, 'w')
    f.write(ip)
    f.close()
 
newip = urllib2.urlopen("http://ip.dnsexit.com/").read().strip()
 
if not os.path.exists(OLDIP_FILE):
    updatedns(newip)
else:
    f = open(OLDIP_FILE, 'r')
    oldip = f.read()
    f.close()
    if oldip != newip:
        updatedns(newip)